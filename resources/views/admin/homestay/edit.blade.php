<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>My Inn - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset("vendor/fontawesome-free/css/all.min.css")}}" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="{{asset("vendor/datatables/dataTables.bootstrap4.css")}}" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset("css1/sb-admin.css")}}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>

    <style type="text/css">
        @keyframes check {0% {height: 0;width: 0;}
            25% {height: 0;width: 10px;}
            50% {height: 20px;width: 10px;}
        }
        .checkbox{background-color:#fff;display:inline-block;height:28px;margin:0 .25em;width:28px;border-radius:4px;border:1px solid #ccc;float:right}
        .checkbox span{display:block;height:28px;position:relative;width:28px;padding:0}
        .checkbox span:after{-moz-transform:scaleX(-1) rotate(135deg);-ms-transform:scaleX(-1) rotate(135deg);-webkit-transform:scaleX(-1) rotate(135deg);transform:scaleX(-1) rotate(135deg);-moz-transform-origin:left top;-ms-transform-origin:left top;-webkit-transform-origin:left top;transform-origin:left top;border-right:4px solid #fff;border-top:4px solid #fff;content:'';display:block;height:20px;left:3px;position:absolute;top:15px;width:10px}
        .checkbox span:hover:after{border-color:#999}
        .checkbox input{display:none}
        .checkbox input:checked + span:after{-webkit-animation:check .8s;-moz-animation:check .8s;-o-animation:check .8s;animation:check .8s;border-color:#555}
        .checkbox input:checked + .default:after{border-color:#444}
        .checkbox input:checked + .primary:after{border-color:#2196F3}
        .checkbox input:checked + .success:after{border-color:#8bc34a}
        .checkbox input:checked + .info:after{border-color:#3de0f5}
        .checkbox input:checked + .warning:after{border-color:#FFC107}
        .checkbox input:checked + .danger:after{border-color:#f44336}

        .main-section{

            margin:0 auto;

            padding: 20px;

            margin-top: 100px;

            background-color: #fff;

            box-shadow: 0px 0px 20px #c1c1c1;

        }

        .fileinput-remove,

        .fileinput-upload{

            display: none;

        }

    </style>

</head>

<body id="page-top">

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="{{route('dashboard')}}">MY INN</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0" style="visibility: hidden;">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
        </li>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">Settings</a>
                <a class="dropdown-item" href="#">Activity Log</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}" data-toggle="modal" data-target="#logoutModal">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </a>
            </div>
        </li>
    </ul>

</nav>

<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="{{route('dashboard')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-folder"></i>
                <span>Users</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <h6 class="dropdown-header">Menu : </h6>
                <a class="dropdown-item" href="{{route('dashboard.user.index')}}">All User</a>
                <a class="dropdown-item" href="{{route('dashboard.user.create')}}">Create User</a>

            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-folder"></i>
                <span>Homestay</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <h6 class="dropdown-header">Menu : </h6>
                <a class="dropdown-item" href="{{route('dashboard.homestay.index')}}">All Homestay</a>
                <a class="dropdown-item" href="{{route('dashboard.homestay.create')}}">Create Homestay</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-folder"></i>
                <span>Fasilitas</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <h6 class="dropdown-header">Menu : </h6>
                <a class="dropdown-item" href="{{route('dashboard.fasilitas.index')}}">All Fasilitas</a>
                <a class="dropdown-item" href="{{route('dashboard.fasilitas.create')}}">Create Fasilitas</a>
            </div>
        </li>
    </ul>

    <div id="content-wrapper">
        <div class="row">
            <div class="col-sm" style="align-items: flex-start;">
                <h1>Edit Homestay</h1>
            </div>
            <div class="col-sm-1">
                <a href="{{route('dashboard.homestay.index', $homestay -> id)}}" class="btn btn-info a-btn-slide-text" style="align-items: flex-end;">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Kembali</strong></span>
                </a>
            </div>
        </div>

        <div class="container-fluid">
            <form action="{{route('dashboard.homestay.update', $homestay -> id)}}" method="post" class="was-validated" enctype="multipart/form-data">
                {{csrf_field()}}
                {{ method_field('PATCH') }}
                <input type = "hidden" name = "user_id" value = "{{Auth::user()->id}}">
                <h3>Edit gambar profil homestay</h3>
                <div class="form-group">
                    <div class="btn btn-primary btn-sm float-left">
                        <input type="file" name="photo_homestay">
                    </div>
                </div>
                <br>
                <br>
                <input type="hidden" class="form-control" id="uname" value="{{$homestay -> unique_id}}" name="unique_id" required readonly>
                <div class="form-group">
                    <label for="uname">Nama Homestay :</label>
                    <input type="text" class="form-control" id="uname" value="{{$homestay -> nama_homestay}}" name="nama_homestay" required>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Harap diisi</div>
                </div>
                <div class="form-group">
                    <label for="uname">Harga :</label>
                    <input type="number" class="form-control" id="uname" value="{{$homestay -> harga}}" name="harga" required>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Harap diisi</div>
                </div>
                <div class="form-group">
                    <label for="uname">Deskripsi :</label>
                    <textarea type="text" class="form-control rounded-0" rows="10" id="uname"  name="deskripsi" required>{{$homestay -> deskripsi}}</textarea>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Harap diisi</div>
                </div>
                <div class="form-group">
                    <label for="sel1">Provinsi :</label>
                    <select class="selectpicker form-control" data-live-search="true" id="sel1" name="provinsi_id">
                        <option value="{{$homestay -> provinsi_id}}">{{$homestay -> provinsi -> name}}</option>
                        <div class="valid-feedback"></div>
                        @foreach($provinsi as $p)
                            <option value="{{$p->id}}">{{$p->name}}</option>
                        @endforeach
                        <div class="invalid-feedback">Harap diisi</div>
                    </select>

                </div>

                <div class="form-group">
                    <label for="sel1">Kota :</label>
                    <select class="selectpicker form-control" data-live-search="true" id="sel1" name="kota_id">
                        <option value="{{$homestay -> kota_id}}">{{$homestay -> kota -> name}}</option>
                        <div class="valid-feedback"></div>
                        @foreach($kota as $k)
                            <option value="{{$k->id}}">{{$k->name}}</option>
                        @endforeach
                        <div class="invalid-feedback">Harap diisi</div>
                    </select>


                </div>
                <div class="form-group">
                    <label for="sel1">Kecamatan :</label>
                    <select class="selectpicker form-control" data-live-search="true" id="sel1" name="district_id">
                        <option value="{{$homestay -> district_id}}">{{!empty($homestay -> kota1 -> name) ? $homestay -> kota1 -> name : 'Tidak ada'}}</option>
                        <div class="valid-feedback"></div>
                        @foreach($distrik as $d)
                            <option value="{{$d->id}}">{{$d->name}}</option>
                        @endforeach
                        <div class="invalid-feedback">Harap diisi</div>
                    </select>
                </div>

                <div class="form-group">
                    <label for="uname">Alamat :</label>
                    <input type="text" class="form-control" id="uname" value="{{$homestay -> alamat}}" name="alamat" required>
                    <div class="valid-feedback"></div>
                    <div class="invalid-feedback">Harap diisi</div>
                </div>

                <h3>Fasilitas Homestay</h3>

                <div class="card" style="margin:30px 0;" >
                    <div class="card-header">Fasilitas Ruangan</div>
                    <ul class="list-group list-group-flush">
                        @foreach($fasilitas_public as $f)
                            <li class="list-group-item">
                                {{$f -> name}}
                                <label class="checkbox">
                                    <input type="checkbox" name="fasilitas_id[]" value="{{$f -> id}}"
                                           @foreach($homestay_detail as $h_detail)
                                               @if($h_detail->fasilitas_id == $f->id)
                                                    checked
                                                @endif
                                            @endforeach
                                        >
                                    <span class="default"></span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="card" style="margin:50px 0;" >
                    <div class="card-header">Fasilitas Kamar</div>
                    <ul class="list-group list-group-flush">
                        @foreach($fasilitas_room as $f)
                            <li class="list-group-item">
                                {{$f -> name}}
                                <label class="checkbox">
                                    <input type="checkbox" name="fasilitas_id[]" value="{{$f -> id}}"
                                           @foreach($homestay_detail  as $h_detail)
                                               @if($h_detail->fasilitas_id == $f->id)
                                               checked
                                               @endif
                                            @endforeach
                                    >
                                    <span class="default"></span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="card" style="margin:50px 0;" >
                    <div class="card-header">Fasilitas Kamar Mandi</div>
                    <ul class="list-group list-group-flush">
                        @foreach($fasilitas_bathroom as $f)
                            <li class="list-group-item">
                                {{$f -> name}}
                                <label class="checkbox">
                                    <input type="checkbox" name="fasilitas_id[]" value="{{$f -> id}}"
                                           @foreach($homestay_detail as $h_detail)
                                               @if($h_detail->fasilitas_id == $f->id)
                                               checked
                                               @endif
                                           @endforeach>
                                    <span class="default"></span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div><div class="card" style="margin:50px 0;" >
                    <div class="card-header">Area dekat homestay</div>
                    <ul class="list-group list-group-flush">
                        @foreach($fasilitas_area as $f)
                            <li class="list-group-item">
                                {{$f -> name}}
                                <label class="checkbox">
                                    <input type="checkbox" name="fasilitas_id[]" value="{{$f -> id}}"
                                           @foreach($homestay_detail as $h_detail)
                                               @if($h_detail->fasilitas_id == $f->id)
                                               checked
                                               @endif
                                           @endforeach
                                    >

                                    <span class="default"></span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <h3>Edit gambar gambar homestay</h3>
                <div>
                    <div class="form-group">
                        <div class="col-sm-9">
                        <span class="btn btn-default btn-file">
                            <div class="file-loading">
                                <input id="file-1" type="file" name="file[]" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
                            </div>
                        </span>
                        </div>
                    </div>
                </div>



                <br>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                        </div>
                        <div class="col">
                            <a href="{{route('dashboard.homestay.index')}}" class="btn btn-primary" style="width: 100%;">Kembali</a>
                        </div>
                    </div>
                </div>
            </form>

            <form action="{{route('dashboard.homestay.destroy', $homestay -> id)}}" method="post" class="was-validated" enctype="multipart/form-data" style="margin-top: 1%;">
                {{csrf_field()}}
                {{ method_field('DELETE') }}
                <input type="hidden" class="form-control" id="uname" value="{{$homestay -> unique_id}}" name="unique_id" required readonly>
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <button type="submit" class="btn btn-danger" style="height: 100%; width: 100%;">Delete</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>



        <script type="text/javascript">

            $("#file-1").fileinput({

                theme: 'fa',

                uploadUrl: "/imageUpload.php",

                allowedFileExtensions: ['jpg', 'png', 'gif'],

                overwriteInitial: false,

                maxFileSize:2000,

                maxFilesNum: 10,

                slugCallback: function (filename) {

                    return filename.replace('(', '_').replace(']', '_');

                }

            });

        </script>
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{asset("vendor/bootstrap/js/bootstrap.bundle.min.js")}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset("vendor/jquery-easing/jquery.easing.min.js")}}"></script>

<!-- Page level plugin JavaScript-->
<script src="{{asset("vendor/chart.js/Chart.min.js")}}"></script>
<script src="{{asset("vendor/datatables/jquery.dataTables.js")}}"></script>
<script src="{{asset("vendor/datatables/dataTables.bootstrap4.js")}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset("js1/sb-admin.min.js")}}"></script>

<!-- Demo scripts for this page-->
<script src="{{asset("js1/demo/datatables-demo.js")}}"></script>
<script src="{{asset("js1/demo/chart-area-demo.js")}}"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>




</body>

</html>

