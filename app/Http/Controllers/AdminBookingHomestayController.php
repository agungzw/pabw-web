<?php

namespace App\Http\Controllers;

use App\Homestay;
use App\Pembayaran;
use App\Wisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminBookingHomestayController extends Controller
{
    //
    public function index(){
        $user = Auth::user();
        if ($user == '') {
            $user = Auth::user();
            return view('user.CariHomeStay', compact('user'));
        }
        else {
            if ($user->role_name == 'Admin') {
                $booking = Pembayaran::all();
                return view('admin.booking.index', compact('booking'));
            } else {
                $user = Auth::user();

                $yogyakarta = Homestay::orderBy('id', 'DESC')->where('provinsi_id', 34)->paginate(3);
                $wisata = Wisata::all();
                return view('user.CariHomeStay', compact('user' ,'yogyakarta', 'wisata'));
            }

        }

    }

    public function edit($id)
    {
        $user = Auth::user();
        if ($user == '') {
            $user = Auth::user();
            return view('user.CariHomeStay', compact('user'));
        }
        else {
            if ($user->role_name == 'Admin') {
                $booking =Pembayaran::findOrFail($id);

                return view('admin.booking.edit', compact('booking'));
            } else {
                $user = Auth::user();
                return view('user.CariHomeStay', compact('user'));
            }

        }

    }

    public function destroy($id)
    {
        $user = Auth::user();
        if ($user == '') {
            $user = Auth::user();
            return view('user.CariHomeStay', compact('user'));
        }
        else {
            if ($user->role_name == 'Admin') {
                $user = Pembayaran::findOrfail($id);
                $user->delete();

                return redirect()->route('dashboard.booking.index')->withSuccess('saved');
            } else {
                $user = Auth::user();
                return view('user.CariHomeStay', compact('user'));
            }

        }

    }
}
