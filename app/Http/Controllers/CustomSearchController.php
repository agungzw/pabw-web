<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;

class CustomSearchController extends Controller
{
    //
    function index(Request $request)
    {
        if(request()->ajax())
        {
            if(!empty($request->filter_country))
            {
                $data = DB::table('homestay')
                    ->select('nama_homestay', 'deskripsi', 'harga', 'provinsi_id', 'kota_id', 'district_id')
                    ->where('kota_id', $request->filter_country)
                    ->get();
            }
            else
            {
                $data = DB::table('homestay')
                    ->select('nama_homestay', 'deskripsi', 'harga', 'provinsi_id', 'kota_id', 'district_id')
                    ->get();
            }
            return datatables()->of($data)->make(true);
        }
        $country_name = City::all();
        return view('custom_search', compact('country_name'));
    }

}
