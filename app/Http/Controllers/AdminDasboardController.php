<?php

namespace App\Http\Controllers;

use App\Homestay;
use App\Wisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminDasboardController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
            if ($user == '') {
                $user = Auth::user();

                $yogyakarta = Homestay::orderBy('id', 'DESC')->where('provinsi_id', 34)->paginate(3);
                $wisata = Wisata::all();
                return view('user.CariHomeStay', compact('user' ,'yogyakarta', 'wisata'));
            }
            else {
                if ($user->role_name == 'Admin') {
                    return view('admin.dashboard.index');
                } else {
                    $user = Auth::user();

                    $yogyakarta = Homestay::orderBy('id', 'DESC')->where('provinsi_id', 34)->paginate(3);
                    $wisata = Wisata::all();
                    return view('user.CariHomeStay', compact('user' ,'yogyakarta', 'wisata'));
                }

            }

    }
}
